import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public email: string = '';
  public password: string = '';

  constructor(private _userService: UsersService, private _router: Router) {}
  
  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    this._userService.login(this.email, this.password).subscribe(result => {
      this._router.navigate(['/home']);
      return;
    });
  }

  public ngOnInit(): void {
  }

}
