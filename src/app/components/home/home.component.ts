import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { FilmListService } from 'src/app/services/movie-list.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private _movies$!: Subscription;

  public movies: Movie[] = [];

  constructor(private _movieList:FilmListService ) { }

  ngOnInit(): void {
    this._movies$ = this._movieList.list().subscribe(movies => this.movies = movies);
  }

  ngOnDestroy(): void {
    this._movies$.unsubscribe();
  }
}
