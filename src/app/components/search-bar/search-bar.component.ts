import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {

  public search!: string;

  constructor(private _searchService: SearchService, private _router: Router) { }

  public handleSearch(form: NgForm): void  {
    if (form.invalid) {
      return;
    }

    this._searchService.searchMovies(this.search).subscribe(movies => {
    this._searchService.setMovies(movies);
    this._router.navigate(['/results']);
    });
  }

  ngOnInit(): void {
  }

}
