import { Component, Input, OnInit } from '@angular/core';
import { SearchMovies } from 'src/app/models/search-movies';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  @Input() public movie!: SearchMovies;

  constructor() { }

  ngOnInit(): void {
  }

}
