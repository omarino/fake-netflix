import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  public confirmPassword: string = "";
  public email: string = "";
  public password: string = "";

  constructor() { }

  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }
  } 

  public ngOnInit(): void {
  }

}
