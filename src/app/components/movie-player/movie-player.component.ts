import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviePlayerService } from 'src/app/services/movie-player.service';

@Component({
  selector: 'app-movie-player',
  templateUrl: './movie-player.component.html',
  styleUrls: ['./movie-player.component.scss']
})
export class MoviePlayerComponent implements OnInit {

  public filmUrl!: string;

  constructor(private _moviePlayer: MoviePlayerService, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this._moviePlayer.getMovie().subscribe(films => {
      console.log(films.videoUrl)
      this.filmUrl = films.videoUrl;
    })
  }

}
