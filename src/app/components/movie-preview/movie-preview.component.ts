import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription, switchMap } from 'rxjs';
import { MoviePreview } from 'src/app/models/movie-preview';
import { MoviePreviewService } from 'src/app/services/movie-preview.service';

@Component({
  selector: 'app-movie-preview',
  templateUrl: './movie-preview.component.html',
  styleUrls: ['./movie-preview.component.scss']
})
export class MoviePreviewComponent implements OnInit {

  private _moviePreview$!: Subscription;

  public favoriteMovies: MoviePreview[] = [];
  public moviePreview!: MoviePreview;

  constructor(private _moviePreview: MoviePreviewService, private _route: ActivatedRoute) { }

  public setFavorite(fav: boolean, id: string) {
    if (fav === false) {
      this._moviePreview.getPreview(id).subscribe(favoriteMovies => {
        this._moviePreview.addFavMovie(favoriteMovies);

        return !fav;
      });
    } else {
      this._moviePreview.getPreview(id).subscribe(() => {
        this._moviePreview.removeFavMovie();

        return !fav;
      });
    }
  }

  ngOnInit(): void {
    this._route.paramMap.pipe(
      map(p => p.get('id') as string),
      switchMap(id => this._moviePreview.getPreview(id))
    ).subscribe(preview => {
      this.moviePreview = preview

      return this.moviePreview = preview;
    })
  }
}