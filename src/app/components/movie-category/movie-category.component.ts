import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription, switchMap } from 'rxjs';
import { Item } from 'src/app/models/item';
import { KeywordService } from 'src/app/services/keyword.service';

@Component({
  selector: 'app-movie-category',
  templateUrl: './movie-category.component.html',
  styleUrls: ['./movie-category.component.scss']
})
export class MovieCategoryComponent implements OnInit {

  private _genres$!: Subscription;

  public genre!: string;
  public movies: Item[] = [];

  constructor(private _movieKeyword: KeywordService, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this._genres$= this._route.paramMap.pipe(
      map(p => p.get('category') as string),
      switchMap(category => this._movieKeyword.getKeyword(category))
    ).subscribe(movie => this.movies = movie); 
  }
}
