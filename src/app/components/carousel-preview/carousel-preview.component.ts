import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs';
import { Item } from 'src/app/models/item';
import { CarouselPreviewService } from 'src/app/services/carousel-preview.service';

@Component({
  selector: 'app-carousel-preview',
  templateUrl: './carousel-preview.component.html',
  styleUrls: ['./carousel-preview.component.scss']
})
export class CarouselPreviewComponent implements OnInit {

  public carousel!: Item;

  constructor(private _carouselService:CarouselPreviewService, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this._route.paramMap.pipe(
      map(c => c.get('id') as string),
      switchMap(id => this._carouselService.getCarousel(id))
    ).subscribe(carousel => this.carousel = carousel);
  }
   
}
