import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SearchMovies } from 'src/app/models/search-movies';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-results-movies',
  templateUrl: './results-movies.component.html',
  styleUrls: ['./results-movies.component.scss'] 
})
export class ResultsMoviesComponent implements OnInit {

  public movies$: Observable<SearchMovies[]> = this._searchService.movies$;

  constructor(private _route: ActivatedRoute, private _searchService: SearchService) { }

  ngOnInit(): void {
  }

}
