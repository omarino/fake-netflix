import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsMoviesComponent } from './results-movies.component';

describe('ResultsMoviesComponent', () => {
  let component: ResultsMoviesComponent;
  let fixture: ComponentFixture<ResultsMoviesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsMoviesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
