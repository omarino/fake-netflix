import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { Subscription } from 'rxjs';
import { Item } from 'src/app/models/item';
import { ComingSoonService } from 'src/app/services/coming-soon.service';
 
@Component({
  selector: 'app-main-slider',
  templateUrl: './main-slider.component.html',
  styleUrls: ['./main-slider.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, noPause: true, showIndicators: true } }
  ]
})
export class MainSliderComponent implements OnInit, OnDestroy {
  
  private _comingSoon$!: Subscription;

  @Input() public movie!: Item;

  public movies: Item[] = [];
  
  constructor(private _comingSoon: ComingSoonService) { }

  ngOnInit(): void {
    this._comingSoon$ = this._comingSoon.comingSoon().subscribe(movies => this.movies = movies)
  }
  
  ngOnDestroy(): void {
    this._comingSoon$.unsubscribe();
  }
}
