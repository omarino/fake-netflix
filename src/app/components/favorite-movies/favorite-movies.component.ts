import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MoviePreview } from 'src/app/models/movie-preview';
import { MoviePreviewService } from 'src/app/services/movie-preview.service';

@Component({
  selector: 'app-favorite-movies',
  templateUrl: './favorite-movies.component.html',
  styleUrls: ['./favorite-movies.component.scss']
})
export class FavoriteMoviesComponent implements OnInit {
  
  public favMovies!: string;
  public favMovies$: Observable<MoviePreview[]> = this._searchService.favMovies$;
  
  constructor(private _searchService: MoviePreviewService) { }
  
  ngOnInit(): void { 
  }

}
