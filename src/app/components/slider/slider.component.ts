import { Component, OnInit, ViewEncapsulation, Input, OnDestroy  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Item } from 'src/app/models/item';
import { KeywordService } from 'src/app/services/keyword.service';
import SwiperCore, { Pagination, Navigation} from "swiper";

SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SliderComponent implements OnInit, OnDestroy {
  
  private _category$!: Subscription;

  @Input() public genre!:string;

  public categories: Item[] = [];

  constructor(private _movieKeyowrd: KeywordService, private _route: ActivatedRoute){ }

  ngOnInit():void { 
     this._category$ = this._movieKeyowrd.getKeyword(this.genre).subscribe( categories =>{
       this.categories = categories;

      return this.categories= categories.splice(0, 15);
     })
  }  

  ngOnDestroy(): void {
    this._category$.unsubscribe();
  }
  
}
