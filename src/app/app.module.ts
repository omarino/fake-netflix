import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForbiddenDirective } from './directives/forbidden.directive';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegexEmailDirective } from './directives/regex-email.directive';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { ResultsMoviesComponent } from './components/results-movies/results-movies.component';
import { MovieComponent } from './components/movie/movie.component';
import { TopMoviesComponent } from './components/top-movies/top-movies.component';
import { MainSliderComponent } from './components/main-slider/main-slider.component';
import { LogoutDirective } from './directives/logout.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { SliderComponent } from './components/slider/slider.component';
import { HomeComponent } from './components/home/home.component';
import { AccessComponent } from './components/access/access.component';
import { MoviePlayerComponent } from './components/movie-player/movie-player.component';
import { UrlSanitizerPipe } from './pipes/url-sanitizer.pipe';
import { CommonModule } from '@angular/common';
import { SwiperModule } from 'swiper/angular';
import { MoviesComponent } from './components/movies/movies.component';
import { HttpClientModule } from '@angular/common/http';
import { CarouselPreviewComponent } from './components/carousel-preview/carousel-preview.component';
import { FavoriteMoviesComponent } from './components/favorite-movies/favorite-movies.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MoviePreviewComponent } from './components/movie-preview/movie-preview.component';
import { MovieCategoryComponent } from './components/movie-category/movie-category.component';

@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    ForbiddenDirective,
    LoginComponent,
    NavbarComponent,
    AccessComponent,
    SliderComponent,
    SearchBarComponent,
    ResultsMoviesComponent,
    MovieComponent,
    RegexEmailDirective,
    SliderComponent,
    TopMoviesComponent,
    LogoutDirective,
    MainSliderComponent,
    SliderComponent,
    HomeComponent,
    MoviePlayerComponent,
    UrlSanitizerPipe,
    MoviesComponent,
    MoviePreviewComponent,
    CarouselPreviewComponent,
    FavoriteMoviesComponent,
    MovieCategoryComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    CarouselModule,
    SwiperModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
