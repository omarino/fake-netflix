import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { emailRegEx } from '../validators/email-regex.validator';

@Directive({
  providers: [{
    multi: true,
    provide:NG_VALIDATORS,
    useExisting: RegexEmailDirective
  }],
  selector: '[appRegexEmail]'
})
export class RegexEmailDirective {

  constructor() { }

  public validate(control: AbstractControl): ValidationErrors | null {
    return emailRegEx()(control);
  }

}
