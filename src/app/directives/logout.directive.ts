import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appLogout]'
})
export class LogoutDirective {

  constructor() { }

  @HostListener('click')
    unloadHandler() {
    window.sessionStorage.clear();
  }

}