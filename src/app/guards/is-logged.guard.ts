import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { STORAGE_KEY_AUTH_TOKEN } from '../services/users.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class IsLoggedGuard implements CanActivate {

  constructor (private _router:Router) {}

  canActivate(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {  
    if (sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN)) {
      return true;
    } else {
      return new Promise((resolve) => {
        this._router.navigate(['/login']);
        return resolve(false);
      });
    }
  };    
}
  
  