import { TestBed } from '@angular/core/testing';

import { MoviePlayerService } from './movie-player.service';

describe('MoviePlayerService', () => {
  let service: MoviePlayerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoviePlayerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

