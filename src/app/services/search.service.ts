import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResults } from '../interfaces/results.interface';
import { ISearchMovie } from '../interfaces/search-movie.interface';
import { SearchMovies } from '../models/search-movies';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private _movies: SearchMovies[] = [];
  private _movies$ = new BehaviorSubject<SearchMovies[]>(this._movies);

  private _objToSearchMovie (obj: ISearchMovie): SearchMovies {
    return new SearchMovies( 
      obj.id,
      obj.image,
      obj.title
    )
  }

  public movies$ = this._movies$.asObservable();

  constructor(private _http: HttpClient) { }

  public addMovie(m: SearchMovies): void {
    this._movies.push(m);
    this._movies$.next(this._movies);
  }

  public addMovies(movies: SearchMovies[]): void {
    this._movies.push(...this._movies);
  }

  public setMovies(movies: SearchMovies[]): void {
    this._movies = movies;
    this._movies$.next(this._movies);
  }
  
  public searchMovies(title: string): Observable<SearchMovies[]> {
    return this._http.get<IResults>(`${environment.api.url}Search/${environment.key}/${title}`).pipe(
      map(result => result.results.map(movie => this._objToSearchMovie(movie)))
    );
  }
}


