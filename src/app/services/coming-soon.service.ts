import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IComingSoon } from '../interfaces/coming-soon.interface';
import { IItem } from '../interfaces/item.interface';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class ComingSoonService {

  private _objToComingSoon (obj:IItem ): Item {
    return new Item( 
      obj.id,
      obj.image,
      obj.title
    )
  }

  constructor(private _http:HttpClient) { }

  public comingSoon(): Observable<Item[]> {
    return this._http.get<IComingSoon>(`${environment.api.url}BoxOffice/${environment.key}`).pipe(
    map(result => result.items.map(movie => this._objToComingSoon(movie)))
    );
  }
}

