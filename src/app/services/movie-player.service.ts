import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { IMoviePlayer } from '../interfaces/movie-player.interface';
import { map, Observable } from 'rxjs';
import { MoviePlayer } from '../models/movie-play';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviePlayerService {

  private _objToModel(obj: IMoviePlayer): MoviePlayer{
    return new MoviePlayer(obj.videoUrl);
  }

  constructor(private _http:HttpClient) { }

   public getMovie(): Observable<MoviePlayer> {
    return this._http.get<IMoviePlayer>(`${environment.api.url}YouTubeTrailer/${environment.key}/tt1375666`).pipe(
      map(url => this._objToModel(url))
    );
  }
}
