import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IItems } from '../interfaces/items.interface';
import { IKeyword } from '../interfaces/keyword.interface';
import { Item } from '../models/item';


@Injectable({
  providedIn: 'root'
})

export class KeywordService {

  private _movies: Item[] = [];
  private _movies$ = new BehaviorSubject<Item[]>(this._movies);

  private _keywordobjToModel (obj:IItems): Item{
    return new Item(
      obj.id,
      obj.image,
      obj.title
    );
  }

  public addMovie(m: Item): void {
    this._movies.push(m);
    this._movies$.next(this._movies);
  }

  public addMovies(movies: Item[]): void {
    this._movies.push(...movies);
    this._movies$.next(this._movies);
  }

  public setMovies(movies: Item[]): void {
    this._movies = movies;
    this._movies$.next(this._movies);
  }

  constructor(private _keywordhttp: HttpClient) { }

  public getKeyword(genre: string): Observable <Item[]>{
    return this._keywordhttp.get<IKeyword>(`${environment.api.url}Keyword/${environment.key}/${genre}`).pipe(
      map(keyword => keyword.items.map(image => this._keywordobjToModel(image)))
    );
  }
}
