import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class CarouselPreviewService {

  private _objToModel(obj:IItem): Item {
    return new Item (
    obj.id,
    obj.image,
    obj.title
    )
  }

  constructor(private _http: HttpClient) { }

  public getCarousel(id: string): Observable<Item> {
    return this._http.get<IItem>(`${environment.api.url}Title/${environment.key}/${id}`).pipe(
    map(carousel => this._objToModel(carousel))
    );
  }
}
