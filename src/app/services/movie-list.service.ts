import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { map } from "rxjs"
import { IMovieList } from "../interfaces/movie-list.interface"
import { Movie } from "../models/movie"
import { environment } from "src/environments/environment"
import { IItems } from "../interfaces/film-list.interface"

@Injectable({
  providedIn: 'root'
})

export class FilmListService {

  private _objToFilm(obj: IMovieList) : Movie {
    return new Movie( 
      obj.id,
      obj.rank,
      obj.title,
      obj.fullTitle,
      obj.year,
      obj.image,
      obj.crew,
      obj.imDbRating,
      obj.imDbRatingCount
    )
  }

  constructor(private _http: HttpClient) { }

   public list() {
    return this._http.get<IItems>(`${environment.api.url}Top250Movies/${environment.key}`).pipe(
      map(movies => movies.items.map(movie => this._objToFilm(movie)))
    )
  }
}