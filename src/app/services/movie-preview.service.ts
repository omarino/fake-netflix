import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IMoviePreview } from '../interfaces/movie-preview.interface';
import { MoviePreview } from '../models/movie-preview';

@Injectable({
  providedIn: 'root'
})
export class MoviePreviewService {

  private _favMovies: MoviePreview[] = [];
  private _favMovies$ = new BehaviorSubject<MoviePreview[]>(this._favMovies);

  public favMovies$ = this._favMovies$.asObservable();

  private _objToModel(obj:IMoviePreview): MoviePreview {
    return new MoviePreview(
      obj.id,
      obj.image,
      obj.title,
      obj.plot,
      obj.genres,
      obj.runtimeMins,
      obj.year
    )
  }

  constructor(private _http:HttpClient) { }

  public addFavMovie(m: MoviePreview): void {
    this._favMovies.push(m);
    this._favMovies$.next(this._favMovies);
  }

  public addFavMovies(movies: MoviePreview[]): void {
    this._favMovies.push(...movies);
    this._favMovies$.next(this._favMovies);
  }

  public removeFavMovie() {
    this._favMovies.splice(this._favMovies.length - 1);
    this._favMovies$.next(this._favMovies);
  }

  public setFavMovies(movies: MoviePreview[]): void {
    this._favMovies = movies;
    this._favMovies$.next(this._favMovies);
  }

  public getPreview(id: string): Observable<MoviePreview> {
    return this._http.get<IMoviePreview>(`${environment.api.url}Title/${environment.key}/${id}`).pipe(
    map(preview => this._objToModel(preview))
    );
  }
}


