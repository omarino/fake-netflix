import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ILogin } from '../interfaces/users.interface';


export const STORAGE_KEY_AUTH_TOKEN = 'auth-token'


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private _http: HttpClient) { }

  public getToken(): string {
    return sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN) as string;
  }

  public hasToken(): boolean {
    return !!sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN);
  }

  public login(email: string, password: string) {

    return this._http.post<ILogin>('https://reqres.in/api/login', {
      username: email,
      password: password
    }, {
      observe: 'response'
    }).pipe(
      map(response => {
        console.log(response);
        if (response.ok) {
          sessionStorage.setItem(STORAGE_KEY_AUTH_TOKEN, response.body?.token as string);
          
          return true;
        }

        return false;
      })
    );
  }
}