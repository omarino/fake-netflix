import { TestBed } from '@angular/core/testing';

import { MoviePreviewService } from './movie-preview.service';

describe('MoviePreviewService', () => {
  let service: MoviePreviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoviePreviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
