import { IItems } from "./items.interface";

export interface IKeyword{
    keyword: string,
    items: IItems[],
}