import { IMovieList } from "./movie-list.interface";

export interface IItems {
    items: IMovieList[];
}