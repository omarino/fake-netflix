import {ISearchMovie } from "./search-movie.interface";

export interface IResults {
    searchType: string;
    expression: string;
    results: ISearchMovie[];
}

