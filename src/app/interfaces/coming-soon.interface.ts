import { IItem } from "./item.interface";

export interface IComingSoon {
    items: IItem[];
}