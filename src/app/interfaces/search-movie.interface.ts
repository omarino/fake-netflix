export interface ISearchMovie {
   id: string;
   resultType: string;
   image: string;
   title: string;
   description: string;
}

