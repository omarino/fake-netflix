export interface IMoviePreview {
    id: string;
    image: string;
    title: string;
    plot: string;
    genres: string;
    runtimeMins: string;
    year: string;
}