export interface IItems {
    id: string,
    image: string, 
    imDbRating: string,
    title: string,
    year: string,
}