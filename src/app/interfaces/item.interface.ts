export interface IItem {
    id: string;
    image: string;
    title: string;
}