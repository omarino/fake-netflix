export class MoviePreview {
    constructor(
       public id: string,
       public image: string,
       public title: string,
       public plot: string,
       public genres: string,
       public runtimeMins: string,
       public year: string,
       public favorite: boolean = false
    ) {
        //
    }
}   