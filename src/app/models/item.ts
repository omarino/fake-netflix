export class Item{
    constructor(
      public id: string,
      public image: string,
      public title: string
    ) {
        //
    }
}
