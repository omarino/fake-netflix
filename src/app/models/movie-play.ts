export class MoviePlayer{
    constructor(
       public videoUrl: string,
       public IMDbid?: string,
       public Title?: string,
       public FullTitle?: string,
       public Type?: string,
       public Year?: string,
       public VideoId?: string,
       public ErrorMessage?: string,
    ){
        //
    }
}


