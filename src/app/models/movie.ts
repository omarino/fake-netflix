export class Movie{
    constructor(
       public id: string,
       public rank: number,
       public title: string,
       public fullTitle: string,
       public year: number,
       public image: string,
       public crew: string,
       public imDbRating: number,
       public imDbRatingCount: number
        )
    {
        //
    }


}