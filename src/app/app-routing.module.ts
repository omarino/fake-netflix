import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoriteMoviesComponent } from './components/favorite-movies/favorite-movies.component';
import { CarouselPreviewComponent } from './components/carousel-preview/carousel-preview.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ResultsMoviesComponent } from './components/results-movies/results-movies.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { MoviePlayerComponent } from './components/movie-player/movie-player.component';
import { MoviePreviewComponent } from './components/movie-preview/movie-preview.component';
import { MovieCategoryComponent } from './components/movie-category/movie-category.component';

const routes: Routes = [{
  component: FavoriteMoviesComponent,
  path: 'favorite'
}, {
  component: ResultsMoviesComponent,
  path: 'results'
}, {
  component: MoviePreviewComponent,
  path: 'preview'
}, {
  component: MovieCategoryComponent,
  path: 'movie-category/:category'
}, { 
  canActivate: [IsLoggedGuard],
  component:HomeComponent,
  path: 'home'
}, {
  component:LoginComponent,
  path: 'login'
}, {
  component:MoviePlayerComponent,
  path: 'movie-player'
}, {
   component:MoviePreviewComponent,
   path: 'movie-preview/:id'
}, {
  component:CarouselPreviewComponent,
  path: 'carousel-preview/:id'
},{
  component:SignUpComponent,
  path: 'sign-up'
}, {
  path: '',
  pathMatch: 'full',
  redirectTo: 'home'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
